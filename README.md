## Work Examples

The following examples of work are projects I led the development process on and also contributed to for user interface design (where listed).

### STEL Design - Website

[<a href="https://steldesign.com">https://steldesign.com</a>]

A WordPress website I developed for STEL, a digital and industrial design studio based in Santa Barbara, CA.


### Joseph's Jewelers

[<a href="http://josephsjewelers.com">http://josephsjewelers.com</a>]

A WordPress website developed for Joseph's Jewelers in Des Moines, Iowa. I also concepted and developed an animation system for SVG line icons. The site's pages are loaded asynchronously and use Javascript's push state functionality to keep the URL's unique.


### Spend Smart. Eat Smart.

[<a href="http://spendsmarteatsmart.org">http://spendsmarteatsmart.org</a>]

A WordPress website for the Iowa State University Office of Extension's meal guidance site. I developed a lazy loading WordPress plugin that allows visitors to scroll through recipe lists without clicking a load more button. The overall structure of the site (mainly the header, navigation, and footer) use a WordPress parent theme as our company does a lot of work for Iowa State University and we maintain the same look and codebase across sites.


### ISU Dining Hours & menus

[<a href="https://dining.iastate.edu/hours-menus/">https://dining.iastate.edu/hours-menus/</a>]

A React JS based map with locations for dining sites at Iowa State Universit. I integrated Google Maps into a time and location based React JS system that displays dining locations for the ISU campus. The application can use the device's location to show locations based on how far away they are, and also shows complete menus for each dining location (if available).


### Bolt - Plugin

[<a href="http://createwithbolt.com">http://createwithbolt.com</a>]

Bolt is a React based WordPress plugin for generating meta fields. This is a personal project of mine that involved UI design, branding, and sole development work. I also created a video in After Effects (and iMovie) to promote the plugin.


### Luno Life - Website

[<a href="https://lunolife.com">https://lunolife.com</a>]

A Shopify based e-commerce website for Luno Life. The add to cart functionality on products and make/model/year selector (found on the homepage) is coded with React JS.